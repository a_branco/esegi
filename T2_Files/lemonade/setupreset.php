<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/loose.dtd">
<html>
<head>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
</head>
<body>
Setting up the DBs.

<?php
include 'includes/dbconfig.inc';
$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die('Error connecting to mysql');
mysql_query("DROP DATABASE " . $dbname,$conn);
mysql_query("CREATE DATABASE ". $dbname,$conn);
echo mysql_error($conn);
include 'includes/db.inc';
$query = 'CREATE TABLE forum_table( '.
		 'cid INT NOT NULL AUTO_INCREMENT, '.
         'poster_name TEXT, '.
         'comment TEXT, '.
		 'date DATETIME, '.
		 'PRIMARY KEY(cid))';	
$result = mysql_query($query);
echo mysql_error($conn );

$query = 'CREATE TABLE accounts( '.
		 'cid INT NOT NULL AUTO_INCREMENT, '.
         'username TEXT, '.
         'password TEXT, '.
		 'sessionid TEXT, '.
		 'PRIMARY KEY(cid))';
$result = mysql_query($query);
echo mysql_error($conn );

$query = 'CREATE TABLE hitlog( '.
		 'cid INT NOT NULL AUTO_INCREMENT, '.
         'hostname TEXT, '.
         'ip TEXT, '.
		 'browser TEXT, '.
		 'referer TEXT, '.
		 'date DATETIME, '.
		 'PRIMARY KEY(cid))';		 
$result = mysql_query($query); 
echo mysql_error($conn );

$query = 'CREATE TABLE products( '.
		 'cid INT NOT NULL AUTO_INCREMENT, '.
         'pcode TEXT, '.
         'price TEXT, '.
		 'description TEXT, '.
		 'PRIMARY KEY(cid))';
$result = mysql_query($query);
echo mysql_error($conn );

// user:admin; password:password
// user:esegi; password:dei
$query = "INSERT INTO accounts (username, password) VALUES
	('admin', '5f4dcc3b5aa765d61d8327deb882cf99'), 
	('esegi', '93319a4441fae08309390fd2e8326002')";
$result = mysql_query($query);
echo mysql_error($conn );

$query ="INSERT INTO `forum_table` (`cid`, `poster_name`, `comment`, `date`) VALUES
	(1, 'adude', 'Hello!!', '2009-03-01 22:26:12'),
	(2, 'adude', 'This lemonade is awesome!', '2009-03-01 22:26:54'),
	(3, 'anonymous', 'I am too good to diclose my identity', '2009-03-01 22:27:11'),
	(4, 'ze', 'Working hard for ESEGI.', '2009-03-01 22:27:48'),
	(5, 'ze', 'Please go to moodle and check the profs latest news.', '2009-03-01 22:29:04'),
	(6, 'manel', 'Got to study for ESEGI. ', '2009-03-01 22:29:49'),
	(7, 'manel', 'This assignment is EASY!', '2009-03-01 22:30:06'),
	(8, 'admin', 'The force IS with me.', '2009-03-01 22:31:13')";
//echo $query;
$result = mysql_query($query);
echo mysql_error($conn );

$query = "INSERT INTO products (pcode, price, description) VALUES
	('SKU01933', '100', 'SafetyWeb'),
	('SKU48585', '100', 'Carbonite 4.0 '),
	('SKU32325', '200', 'McAfee Total Protection 2011'),
	('SKU94855', '500', 'McAfee AntiVirus Plus 2011'),
	('SKU99595', '50', 'Saavi Accountability'),
	('SKU95959', '1000', 'AVG Anti-Virus Free 2011'),
	('SKU95585', '1000', 'F-Secure Internet Security 2011'),
	('SKU09484', '5000', 'Norton Internet Security 2011'),
	('SKU67893', '100', 'Trend Micro Titanium Antivirus + 2011'),
	('SKU67893', '10', 'Panda Internet Security 2011'),
	('SKU09484', '5000', 'Panda Antivirus Pro 2011'),
	('SKU67893', '100', 'BitDefender Antivirus Pro 2011'),
	('SKU67893', '10', 'Kaspersky Internet Security 2011'),
	('SKU67893', '10', 'Emsisoft Anti-Malware 5.0'),
	('SKU67893', '10', 'ZoneAlarm Free Firewall 9.2'),
	('SKU67893', '10', 'Comodo Internet Security Suite 4.0'),
	('SKU67893', '10', 'VIPRE Antivirus Premium 4.0'),
	('SKU86522', '50', 'Webroot Internet Security Complete 2011')";
$result = mysql_query($query);
echo mysql_error($conn );

echo "<p>If you see no errors above, it should be done. <a href=\"index.php\">Continue back to the frontpage.</a>";
?>
</body>
</html>
