<?php

// User session management code

function verifyLoginFields($username, $password)
{
	global $login_register_result;
	if(!$username) {
		$login_register_result = "You must supply a username.";
		return false;
	}
	else if(!$password) { 
		$login_register_result = "You must supply a password.";
		return false;
	}
	return true;
}

class User {
  var $id = 0; // the current user's id
  var $username = null;
  var $cookieName = "LemonadeLogin";
  
  function User() {
    if ( isset($_COOKIE[$this->cookieName]) ) {
      $this->_checkLoginCookie($_COOKIE[$this->cookieName]);
    }
  } 

  function _setLoginCookie() {
	if ($this->id == 0 || $this->username == null) return false;
	$sid = md5($this->username.mt_rand());
	$arr = array($this->id, $sid);
	$cookieData = base64_encode(serialize($arr));
	setcookie($this->cookieName, $cookieData, time() + 7776000); // cookie expires in 90 days
	$sql = "UPDATE accounts SET sessionid = '$sid' " .
          "WHERE cid = $this->id";
	execute_query($sql);
	return true;
  } 

  function _login($username, $password) {
	$md5pass = md5($password);
	$query  = "SELECT * FROM accounts WHERE username='". $username ."' AND password='". $md5pass ."'";
	$result = execute_query($query);
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		$this->id = $row['cid'];
		$this->username = $row['username'];
		$this->_setLoginCookie();
	} else {
		return false;
	}
	return true;
  } 
  
  function _register($username, $password) {
		$query  = "SELECT * FROM accounts WHERE username='". $username . "'";
		$result = execute_query($query);
		if (mysql_num_rows($result) != 0) return false;
		$md5pass = md5($password);
		$query = "INSERT INTO accounts (username, password) VALUES
			('" . $username ."', '" . $md5pass . "')";
		$result = execute_query($query);
		if (!$result) return false;
			
		return $this->_setLoginCookie();
  }  
  
  function _logout() {
    if(isset($_COOKIE[$this->cookieName])) setcookie($this->cookieName);
    $this->id = 0;
    $this->username = null;
  }
  
  function _checkLoginCookie() {
    if ( !isset($_COOKIE[$this->cookieName]) ) return;  
    $arr = unserialize(base64_decode($_COOKIE[$this->cookieName]));
    list($uid, $sid) = $arr;
    if (!$uid or !$sid) {
		$this->id = 0;
		$this->username = null;
	} else {
		$query  = "SELECT * FROM accounts WHERE cid='".$uid."' AND sessionid='".$sid."'";		
		$result = execute_query($query);
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			$this->id = $row['cid'];
			$this->username = $row['username'];
		} else {
			$this->id = 0;
			$this->username = null;
		}
	}
  }
}
?>
